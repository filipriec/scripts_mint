#!/bin/bash

TOGGLE_FILE="$HOME/.toggle_battery_label"

# Function to get battery status and percentage
get_battery_info() {
    # Use acpi to get battery status and percentage
    read -r status percentage <<< $(acpi -b | awk -F', ' '{print $1, $2}' | awk '{print $3, $4}' | tr -d '%,')

    # Determine the battery icon based on the current percentage level
    if [[ $status = *Charging* ]]; then
        if [ "$percentage" -ge 80 ]; then
            icon="󱊦"  # nf-md-battery_charging_high
        elif [ "$percentage" -ge 60 ]; then
            icon="󱊥"  # nf-md-battery_charging_medium
        elif [ "$percentage" -ge 40 ]; then
            icon="󱊤"  # nf-md-battery_charging_low
        else
            icon="󰢟"  # nf-md-battery_charging_outline
        fi
    else
        if [ "$percentage" -ge 80 ]; then
            icon=""  # Battery full
        elif [ "$percentage" -ge 60 ]; then
            icon=""  # Battery 3/4
        elif [ "$percentage" -ge 40 ]; then
            icon=""  # Battery 1/2
        elif [ "$percentage" -ge 20 ]; then
            icon=""  # Battery 1/4
        else
            icon=""  # Battery empty
        fi
    fi

    # If the toggle file exists, only show the percentage, otherwise show the icon with percentage
    if [ -f "$TOGGLE_FILE" ]; then
        echo "$percentage%"
    else
        echo "$icon"
    fi
}

# Output the battery information
get_battery_info
