#!/bin/bash

brightness=$(brightnessctl g | cut -d '(' -f 2 | cut -d ')' -f 1)

if [ $brightness -ge 0 ] && [ $brightness -le 30 ]; then
    icon=$'󰃝'  # Dimmest (nf-md-brightness_5)
elif [ $brightness -gt 30 ] && [ $brightness -le 80 ]; then
    icon=$'󰃞'  # Dim (nf-md-brightness_6)
elif [ $brightness -gt 80 ] && [ $brightness -le 400 ]; then
    icon=$'󰃟'  # Moderate (nf-md-brightness_7)
elif [ $brightness -gt 400 ] && [ $brightness -le 650 ]; then
    icon=$'󰃠'  # Bright (nf-md-brightness_7)
elif [ $brightness -gt 650 ] && [ $brightness -le 900 ]; then
    icon=$'󰃠'  # Brighter (nf-md-brightness_7)
else
    icon=$'󰃠'  # Brightest (nf-md-brightness_7)
fi

echo "$icon"
