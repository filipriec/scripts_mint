#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Launch all three bars
polybar --config=~/.config/polybar/config.ini bar1 &
polybar --config=~/.config/polybar/config.ini bar2 &
polybar --config=~/.config/polybar/config.ini bar3 &
