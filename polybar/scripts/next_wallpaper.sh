#!/usr/bin/env bash

COUNTER_FILE="$HOME/.wallpaper-counter"
LAST_WALLPAPER_FILE="$HOME/.last-wallpaper"
WALLPAPER_DIR="$HOME/wallpaper"

# Get a list of all wallpapers, exclude lock.png
WALLPAPERS=($(find $WALLPAPER_DIR -type f \( -iname "*.jpg" -o -iname "*.png" \) ! -iname "lock.png"))
NUM_WALLPAPERS=${#WALLPAPERS[@]}

# Read the counter
COUNTER=$(cat $COUNTER_FILE)

# Increment the counter
COUNTER=$(( (COUNTER + 1) % NUM_WALLPAPERS ))

# Update the counter file
echo $COUNTER > $COUNTER_FILE

# Set the new wallpaper
feh --bg-fill "${WALLPAPERS[$COUNTER]}"

# Store the last wallpaper
echo "${WALLPAPERS[$COUNTER]}" > $LAST_WALLPAPER_FILE

