#!/bin/bash

# Loop over all open windows
wmctrl -lx | awk '{print $1,$3}' | while read -r id class; do
    # Extract the application name from the window class
    appname=$(echo $class | awk -F '.' '{print $2}' | awk '{print tolower($0)}')

    # Format each application name as a clickable Polybar action
    echo -n "%{A1:wmctrl -ia $id:}$appname%{A}  "
done

echo
