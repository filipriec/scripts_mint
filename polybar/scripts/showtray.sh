#!/usr/bin/env bash

##old
#polybar traybar &
#sleep 10
#kill $(pgrep polybar | tail -n 1)
##old

# Path to a file where the Polybar tray PID will be stored
PID_FILE="/tmp/polybar_tray.pid"

# Check if the PID file exists and if the process is running
if [ -f "$PID_FILE" ] && kill -0 $(cat "$PID_FILE") 2>/dev/null; then
  # If so, kill the process and remove the PID file
  kill $(cat "$PID_FILE") && rm -f "$PID_FILE"
else
  # Otherwise, launch polybar and store its PID
  polybar traybar & echo $! > "$PID_FILE"
  # After 10 seconds, remove the PID file if the user hasn't already closed the tray
  sleep 10 && [ -f "$PID_FILE" ] && kill $(cat "$PID_FILE") && rm -f "$PID_FILE"
fi
