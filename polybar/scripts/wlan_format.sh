#!/bin/bash

if [ -f "/tmp/polybar_messaging_wlan" ]; then
  rm "/tmp/polybar_messaging_wlan"
  exec polybar-msg hook wlan 2
fi

TOGGLE_FILE="$HOME/.toggle_wlan_label"

get_signal_strength() {
  local strength=$(iwconfig wlp9s0 | grep 'Link Quality' | awk -F'=' '{print $2}' | awk '{print $1}')
  local level=$(echo "$strength" | cut -d'/' -f1)
  local max=$(echo "$strength" | cut -d'/' -f2)
  local percent=$(( ($level * 100) / $max ))
  echo $percent
}

if [ -f "$TOGGLE_FILE" ]; then
  essid=$(iwgetid -r)
  echo "%{A1:~/.config/polybar/scripts/toggle_module_label.sh wlan:} $essid %{A}"
else
  strength=$(get_signal_strength)
  icon="."
  if [ "$strength" -ge 80 ]; then
    icon=""  # Signal 4
  elif [ "$strength" -ge 60 ]; then
    icon=""  # Signal 3
  elif [ "$strength" -ge 40 ]; then
    icon=""  # Signal 2
  elif [ "$strength" -ge 20 ]; then
    icon="."  # Signal 1
  fi
  echo "%{A1:~/.config/polybar/scripts/toggle_module_label.sh wlan:} $icon %{A}"
fi
