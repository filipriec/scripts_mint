#!/bin/bash

# Get the currently focused workspace
workspace=$(i3-msg -t get_workspaces | jq '.[] | select(.focused==true).name' | tr -d '"')

# Run the provided command
$1 &

# Get the window ID of the most recently launched program
win_id=$(xdotool getactivewindow)

# Move the program to the initially focused workspace
i3-msg [id="$win_id"] move container to workspace $workspace
