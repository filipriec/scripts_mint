#!/usr/bin/env bash

LAST_WALLPAPER_FILE="$HOME/.last-wallpaper"

# Check if the last wallpaper file exists, if not, exit
if [ ! -f $LAST_WALLPAPER_FILE ]; then
  echo "No last wallpaper file found."
  exit 1
fi

# Set the wallpaper to the last one set by next_wallpaper.sh
feh --bg-fill "$(cat $LAST_WALLPAPER_FILE)"
