#!/usr/bin/env bash

WALLPAPER_DIR="$HOME/wallpaper"
COUNTER_FILE="$HOME/.wallpaper-counter"
LAST_WALLPAPER_FILE="$HOME/.last-wallpaper"

# Create counter file if it doesn't exist
if [ ! -f $COUNTER_FILE ]; then
  echo "0" > $COUNTER_FILE
fi

# Read the counter from the file
COUNTER=$(cat $COUNTER_FILE)

# Get a list of all wallpapers, exclude lock.png
WALLPAPERS=($(find $WALLPAPER_DIR -type f \( -iname "*.jpg" -o -iname "*.png" \) ! -iname "lock.png"))
NUM_WALLPAPERS=${#WALLPAPERS[@]}

# If the last wallpaper is stored, set it
if [ -f $LAST_WALLPAPER_FILE ]; then
    feh --bg-fill "$(cat $LAST_WALLPAPER_FILE)"
fi

# Set the wallpaper
feh --bg-fill "${WALLPAPERS[$COUNTER]}"

# Store the last wallpaper
echo "${WALLPAPERS[$COUNTER]}" > $LAST_WALLPAPER_FILE

# Update the counter
COUNTER=$(( (COUNTER + 1) % NUM_WALLPAPERS ))
echo $COUNTER > $COUNTER_FILE
